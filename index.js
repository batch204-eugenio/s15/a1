console.log("Hello World!");
// First name and Last name.
let firstName = "Mark Roland";
let lastName = "Eugenio";
console.log(firstName);
console.log(lastName);
// Age.
let age = 30;
console.log(30);
// Hobbies as Array.
let hobbies = ["Chess", "Swimming", "Painting", "singing" ];
console.log(hobbies);
// Work address as objects.
let workAddress = {
	houseNumber: "Block 12 Number 8.",
	streetName: "Upo Street",
	city: "Marikina City",
	state: "Philippines"
};
console.log(workAddress);

// Log the values of each variable to follow/mimic the output.
let fullName = "Mark Roland Eugenio"
console.log("My full name is " + fullName);

let myAge = 30;
console.log("My current age is " + myAge);

let myFriends = ["April", "May", "June", "July"];
console.log(myFriends);

// Profile

let profile = {
	username: "Mark Hodler",
	fullName: "Mark Roland Eugenio",
	age: 30,
	isActive: true,
};
console.log("My Full Profile Details " + profile);

let realName = "Brethren";
console.log("My bestfriend is: " + realName);

const presentLocation = ["Marikina City"];
presentLocation[0] = "Davao City";
console.log("I am currenty residing in " + presentLocation[0]);
